package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    public Todo findById(Long id) {
        return todoRepository.findById(id)
                .orElse(null);
    }

    public Todo create(Todo todo) {
        todo.setDone(false);
        return todoRepository.save(todo);
    }

    public Todo update(Long id, Todo todo) {
        Todo toBeUpdatedTodo = todoRepository.findById(id)
                .orElse(null);
        if (todo.getText() != null && !todo.getText().equals(toBeUpdatedTodo.getText())) {
            toBeUpdatedTodo.setText(todo.getText());
        } else {
            toBeUpdatedTodo.setDone(!toBeUpdatedTodo.getDone());
        }
        return todoRepository.save(toBeUpdatedTodo);
    }

    public boolean delete(Long id) {
        todoRepository.deleteById(id);
        return true;
    }
}
