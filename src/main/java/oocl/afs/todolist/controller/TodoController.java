package oocl.afs.todolist.controller;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        return TodoMapper.toResponse(todoService.findById(id));
    }

    @PostMapping
    TodoResponse create(@RequestBody TodoCreateRequest todoCreateRequest){
        Todo todo = TodoMapper.toEntity(todoCreateRequest);
        return TodoMapper.toResponse(todoService.create(todo));
    }

    @PutMapping("/{id}")
    TodoResponse update(@PathVariable Long id, @RequestBody TodoCreateRequest todoCreateRequest){
        Todo todo = TodoMapper.toEntity(todoCreateRequest);
        return TodoMapper.toResponse(todoService.update(id, todo));
    }

    @DeleteMapping("/{id}")
    boolean delete(@PathVariable Long id){
        return todoService.delete(id);
    }
}
